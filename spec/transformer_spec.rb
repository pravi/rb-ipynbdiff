# frozen_string_literal: true

require 'rspec'
require 'ipynbdiff'
require 'json'

BASE_PATH = 'spec/testdata/'

def load_test(test_case, test_config)

  input_path = "#{BASE_PATH}#{test_case}/input.ipynb"

  input = if File.exist? input_path
            File.read(input_path)
          else
            File.read('spec/testdata/from.ipynb')
          end

  [
    IpynbDiff::Transformer.new(**test_config).transform(input),
    File.read("#{BASE_PATH}#{test_case}/expected.md")
  ]
end

def config(include_metadata: false, cell_decorator: :html)
  {
    include_metadata: include_metadata,
    cell_decorator: cell_decorator
  }
end

describe IpynbDiff::Transformer do
  {
    'When there are no cells' => ['should render metadata', 'no_cells', config(include_metadata: true)],
    'When there are no cells, but metadata is false' => ['should generate empty document', 'no_cells_no_metadata'],
    'When there is a markdown cell' => ['adds a markdown block', 'only_md'],
    'When there is only one line of markdown' => ['adds a markdown block', 'single_line_md'],
    'When there is a raw cell' => ['adds a raw block', 'only_raw'],
    'When there is a code cell, but no output' => ['adds a code block', 'only_code'],
    'When there is a code cell, but no language' => ['does not add language', 'only_code_no_language'],
    'When there is a code cell, but no kernelspec' => ['does not add language', 'only_code_no_kernelspec'],
    'When there is a code cell, but no nb metadata' => ['does not add language', 'only_code_no_metadata'],
    'When there is a text output' => ['extracts the text', 'text_output'],
    'When there is an html output' => ['ignores it', 'ignore_html_output'],
    'When there is a png output' => ['extracts it along with the text', 'text_png_output'],
    'When there is an svg output' => ['embeds it as image', 'svg'],
    'When there is a latex output' => ['extracts it along with the text', 'latex_output'],
    'When there is an error output' => ['extracts it along with the code', 'error_output'],
    'When there is not metadata on a cell' => ['should not fetch tags', 'no_metadata_on_cell'],
    'When using :percent decorator' => ['generates the correct output', 'percent_decorator', config(cell_decorator: :percent)],
  }.each do |describe_desc, test_case|
    describe describe_desc do
      it test_case[0] do
        converted, expected = load_test(test_case[1], test_case[2] || config)
        expect(converted).to eq expected
      end
    end
  end

  context 'When the notebook is invalid' do
    [
      ['because the json is invalid', 'a'],
      ['because it doesnt have the cell tag', '{"metadata":[]}']
    ].each do |ctx, notebook|
      context ctx do
        it 'raises error' do
          expect do
            IpynbDiff::Transformer.new.transform(notebook)
          end.to raise_error(IpynbDiff::InvalidNotebookError)
        end
      end
    end
  end
end
