<div class="cell code" data-id="5" data-tags="">

``` python
from IPython.display import display, Math
display(Math(r'Dims: {}x{}m \\ Area: {}m^2 \\ Volume: {}m^3'.format(1, round(2,2), 3, 4)))
```

<div class="output display_data">

    $\displaystyle Dims: 1x2m \\ Area: 3m^2 \\ Volume: 4m^3$

</div>

</div>
