<div class="cell code" data-id="5" data-tags="senoid">

``` python
x = np.linspace(0, 4*np.pi,50)
y = 2 * np.sin(x)

plt.plot(x, y)
```

<div class="output execute_result">

    [<matplotlib.lines.Line2D at 0x12a4e43d0>]

</div>

<div class="output display_data">

    ![](data:image/png;base64,this_is_an_invalid_hash_for_testing_purposes)

</div>

</div>
