<div class="cell code" data-id="5" data-tags="senoid">

``` python
plt.plot(x, y)
```

<div class="output execute_result">

    [<matplotlib.lines.Line2D at 0x12a4e43d0>]

</div>

</div>
