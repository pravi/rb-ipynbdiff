<div class="cell code" data-id="5" data-tags="some-table">

``` python
df[:2]
```

<div class="output execute_result">

              x         y
    0  0.000000  0.000000
    1  0.256457  0.507309

</div>

</div>
