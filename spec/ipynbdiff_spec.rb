# frozen_string_literal: true

require 'ipynbdiff'
require 'rspec'

describe IpynbDiff do
  from_path = 'spec/testdata/from.ipynb'
  to_path = 'spec/testdata/to.ipynb'

  describe 'diff' do
    context 'when output type is html' do
      it 'generates html' do
        d = IpynbDiff.diff(from_path, to_path, format: :html, sources_are_files: true)

        expect(d).to include('<li')
      end
    end

    context 'if preprocessing is off' do
      it 'generates regular diff' do
        d = IpynbDiff.diff(from_path, to_path, preprocess_input: false, sources_are_files: true)

        expect(d).to include('<td>')
      end
    end

    context 'if preprocessing is active' do
      it 'html tables are stripped' do
        d = IpynbDiff.diff(from_path, to_path, sources_are_files: true)

        expect(d).to_not include('<td>')
      end
    end

    context 'an output path is passed' do

      before do
        random_file_name = (0...8).map { rand(65..90).chr }.join

        @test_file = "/tmp/#{random_file_name}.txt"
      end

      it 'it creates a new file for the output' do

        d = IpynbDiff.diff(from_path, to_path, sources_are_files: true, write_output_to: @test_file,
                                               diff_opts: { context: 3, include_diff_info: true })

        expect(File.read(@test_file)).to eq(d)
      end

      after do
        File.delete(@test_file)
      end
    end

    context 'when either notebook nil' do

      def test_for_nil_input(from, to, sign)
        lines = IpynbDiff.diff(from, to, sources_are_files: true, include_metadata: false).scan(/.*\n/)

        expect(lines.length).to eq(7)
        expect(lines).to all(start_with(sign))
      end

      it 'should consider from as an empty string' do
        test_for_nil_input(nil, 'spec/testdata/only_md/input.ipynb', '+')
      end

      it 'should consider to as an empty string' do
        test_for_nil_input('spec/testdata/only_md/input.ipynb', nil, '-')
      end
    end

    context 'and setting include_metadata to true' do
      it 'should show changes metadata in the metadata' do
        d = IpynbDiff.diff(from_path, to_path, preprocess_input: true,
                                               sources_are_files: true,
                                               transform_options: { include_metadata: true })

        expect(d).to include('+    display_name: New Python 3 (ipykernel)')
      end
    end

    context 'and setting include_metadata to false' do
      it 'should drop metadata from the diff' do
        d = IpynbDiff.diff(from_path, to_path, preprocess_input: true, sources_are_files: true,
                                               transform_options: { include_metadata: false })

        expect(d).to_not include('+    display_name: New Python 3 (ipykernel)')
      end
    end

    context 'when either notebook is invalid' do
      [
        ['because the json is invalid', 'a'],
        ['because theres no metadata', '{"cells":[]}'],
        ['because it doesnt have the cell tag', '{"metadata":[]}']
      ].each do |ctx, notebook|
        context ctx do
          it 'should return nil' do
            expect(IpynbDiff.diff(notebook, '')).to be_nil
            expect(IpynbDiff.diff('', notebook)).to be_nil
          end
        end
      end
    end
  end

  describe 'transform' do
    context 'when the notebook is invalid' do
      [
        ['because the json is invalid', 'a'],
        ['because it doesnt have the cell tag', '{"metadata":[]}']
      ].each do |ctx, notebook|
        context ctx do
          it 'should return nil' do
            expect(IpynbDiff.transform(notebook)).to be_nil
          end
        end
      end
    end

    context 'when include_metadata is true' do
      it 'keeps metadata in the transformation' do
        from = File.read(from_path)
        transformed = IpynbDiff.transform(from, options: { include_metadata: true })

        expect(transformed).to include('display_name: Python 3 (ipykernel)')
      end
    end

    context 'when include_metadata is false' do
      it 'drops metadata from the transformation' do
        from = File.read(from_path)
        transformed = IpynbDiff.transform(from, options: { include_metadata: false })

        expect(transformed).to_not include('display_name: Python 3 (ipykernel)')
      end
    end
  end

end
