---
jupyter:
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
  language_info:
    codemirror_mode:
      name: ipython
      version: 3
    file_extension: ".py"
    mimetype: text/x-python
    name: python
    nbconvert_exporter: python
    pygments_lexer: ipython3
    version: 3.9.7
  nbformat: 4
  nbformat_minor: 5
---

<div class="cell markdown" data-id="0aac5da7-745c-4eda-847a-3d0d07a1bb9b" data-tags="">

# This is a markdown cell

This paragraph has
With
Many
Lines. How we will he handle MR notes?

But I can add another paragraph

</div>

<div class="cell raw" data-id="faecea5b-de0a-49fa-9a3a-61c2add652da" data-tags="">

This is a raw cell
With
Multiple lines

</div>

<div class="cell code" data-id="893ca2c0-ab75-4276-9dad-be1c40e16e8a" data-tags="">

``` python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
```

</div>

<div class="cell code" data-id="0d707fb5-226f-46d6-80bd-489ebfb8905c" data-tags="">

``` python
np.random.seed(42)
```

</div>

<div class="cell code" data-id="35467fcf-28b1-4c7b-bb09-4cb192c35293" data-tags="senoid">

``` python
x = np.linspace(0, 4*np.pi,50)
y = np.sin(x)

plt.plot(x, y)
```

<div class="output execute_result">

    [<matplotlib.lines.Line2D at 0x123e39370>]

</div>

<div class="output display_data">

![](data:image/png;base64,some_invalid_base64_image_here)

</div>

</div>

<div class="cell code" data-id="dc1178cd-c46d-4da3-9ab5-08f000699884" data-tags="">

``` python
df = pd.DataFrame({"x": x, "y": y})
```

</div>

<div class="cell code" data-id="6e749b4f-b409-4700-870f-f68c39462490" data-tags="some-table">

``` python
df[:2]
```

<div class="output execute_result">

              x         y
    0  0.000000  0.000000
    1  0.256457  0.253655

</div>

</div>

<div class="cell code" data-id="0ddef5ef-94a3-4afd-9c70-ddee9694f512" data-tags="">

``` python

```

</div>
